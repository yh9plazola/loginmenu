import { collection, addDoc } from 'https://www.gstatic.com/firebasejs/10.5.2/firebase-firestore.js';
import { ref, uploadBytes, getDownloadURL } from 'https://www.gstatic.com/firebasejs/10.5.2/firebase-storage.js';
import { storage } from '../app/firebase.js';

export const agregarProducto = async (img) => {
  try {
    const storageRef = ref(storage, 'imagenes/' + img.name);
    await uploadBytes(storageRef, img);
  } catch (error) {
    console.error('Error al agregar el documento:', error);
  }
};