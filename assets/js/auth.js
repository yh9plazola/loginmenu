import { auth } from '../app/firebase.js';
import { onAuthStateChanged } from 'https://www.gstatic.com/firebasejs/10.5.2/firebase-auth.js';
import { signOut } from 'https://www.gstatic.com/firebasejs/10.5.2/firebase-auth.js';

onAuthStateChanged(auth, (user) => {
    if (!user) {
      // El usuario no está autenticado, redirigir a la página de inicio de sesión
      window.location.href = '../../index.html';
    }
});

const back = document.querySelector('.back');

back.addEventListener('click', e => {
	cerrarSesion();
});

const cerrarSesion = () => {
	signOut(auth).then(() => {
		console.log('Sesión cerrada exitosamente');
		document.location.assign('../../index.html');
	}).catch((error) => {
		console.error('Error al cerrar sesión:', error);
	});
}