import { sendPasswordResetEmail } from "https://www.gstatic.com/firebasejs/10.5.2/firebase-auth.js";
import { auth } from "../app/firebase.js";

const formulario = document.querySelector('.form');
const warning = document.querySelector('.warning');

formulario.addEventListener('submit', async e => {
    e.preventDefault();
    const email = formulario['email'].value;
    try {
        await sendPasswordResetEmail(auth, email);
        warning.classList.remove('active');
        warning.classList.add('message');
        warning.innerText = "Contraseña de reseteo enviada con éxito. Revise su correo electrónico.";
    } catch (error) {
        warning.classList.remove('message');
        warning.classList.add('active');
        console.log(error);
        if (error.code) {
            switch (error.code) {
                case 'auth/user-not-found':
                    warning.innerText = 'Este usuario no existe.';
                    break;
                case 'auth/invalid-email':
                    warning.innerText = 'Correo Electrónico invalido.';
                    break;
                default:
                    warning.innerText = 'Error no manejado: ' + error.code;
            }
        }
    }
});