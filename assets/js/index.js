const temp_login = document.querySelector('#temp-login');
const temp_signup = document.querySelector('#temp-signup');
const fragment = document.createDocumentFragment();

const warning = document.querySelector('.warning');

const changeForm = (clone) => {
    warning.classList.remove('active');
    const form = document.querySelector('.form');
    const field = document.querySelector('#field');
    if(field) field.parentNode.removeChild(field);
    fragment.appendChild(clone);
    form.appendChild(fragment);
}

changeForm(temp_login.content.firstElementChild.cloneNode(true));

// Eventos 
document.addEventListener('click', (e) => {
    switch(e.target.className){
        case ('link-signup'):
            changeForm(temp_signup.content.firstElementChild.cloneNode(true));
            break;
        case ('link-login'):
            changeForm(temp_login.content.firstElementChild.cloneNode(true));
            break;
    }
});