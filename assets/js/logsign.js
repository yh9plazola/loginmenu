import { createUserWithEmailAndPassword } from "https://www.gstatic.com/firebasejs/10.5.2/firebase-auth.js";
import { signInWithEmailAndPassword } from "https://www.gstatic.com/firebasejs/10.5.2/firebase-auth.js";
import { auth } from "../app/firebase.js";

const formulario = document.querySelector('.form');
const warning = document.querySelector('.warning');

formulario.addEventListener('submit', async e => {
    e.preventDefault();
    const email = formulario['email'].value;
    const pass = formulario['pass'].value;
    try {
        if(document.querySelector('.form .btn-send').value === 'Create account') {
            if(pass === formulario['cpass'].value) {
                await createUserWithEmailAndPassword(auth, email, pass);
                warning.classList.remove('active');
                document.location.assign('../../index.html');
            }
            else {
                warning.classList.add('active');
                warning.innerText = 'Las contraseñas no coinciden.';
                return;
            }
        }
        else {
            await signInWithEmailAndPassword(auth, email, pass);
            document.location.assign('../assets/html/logged.html');
        }
    } catch (error) {
        warning.classList.add('active');
        if(document.querySelector('.form .btn-send').value === 'Create account') {
            if (error.code) {
                switch (error.code) {
                    case 'auth/email-already-in-use':
                        warning.innerText = 'El correo electrónico ya está en uso.';
                        break;
                    case 'auth/invalid-email':
                        warning.innerText = 'El correo electrónico proporcionado no es válido.';
                        break;
                    case 'auth/weak-password':
                        warning.innerText = 'La contraseña es débil. Proporcione una contraseña más fuerte.';
                        break;
                    default:
                        warning.innerText = 'Error no manejado: ' + error.code;
                }
            }
        }
        else {
            if (error.code) {
                switch (error.code) {
                    case 'auth/invalid-login-credentials':
                        warning.innerText = 'Usuario no encontrado o contraseña incorrecta. Regístrese en el enlace de arriba.';
                        break;
                    default:
                        warning.innerText = 'Error no manejado: ' + error.code;
                }
            }
        }
    }
});