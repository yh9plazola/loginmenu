// Import the functions you need from the SDKs you need
import { initializeApp } from "https://www.gstatic.com/firebasejs/10.5.2/firebase-app.js";
import { getAuth } from "https://www.gstatic.com/firebasejs/10.5.2/firebase-auth.js";
import { getStorage } from "https://www.gstatic.com/firebasejs/10.5.2/firebase-storage.js";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyD2cfIqLEP3hWTsEoswEqNjmNrgHGp28Ik",
  authDomain: "login-ddc12.firebaseapp.com",
  projectId: "login-ddc12",
  storageBucket: "login-ddc12.appspot.com",
  messagingSenderId: "1038632245329",
  appId: "1:1038632245329:web:288499ed155c7aca9f8070"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
export const auth = getAuth(app);
export const storage = getStorage(app);