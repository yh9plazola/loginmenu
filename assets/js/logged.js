import { agregarProducto } from "./add.js";

const btn_send = document.querySelector('.btn-send');
const warning = document.querySelector('.warning');

btn_send.addEventListener('click', async (e) => {
    e.preventDefault();
    const file = document.querySelector('#file');
    try{
        await agregarProducto(file.files[0]);
    }
    catch (error) {
        warning.classList.remove('message');
        warning.classList.add('active');
        warning.innerText = "Ocurrió un error.";
        return;
    }
    warning.classList.remove('active');
    warning.classList.add('message');
    warning.innerText = "Se subió la imagen con éxito";
});